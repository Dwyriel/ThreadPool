#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <mutex>
#include <queue>
#include <thread>
#include <condition_variable>
#include <functional>

class ThreadPool {
    bool shouldStop = false;
    bool joined = false;
    std::mutex threadMutex;
    std::condition_variable threadMutex_condition;
    std::condition_variable joinMutex_condition;
    std::vector<std::thread> threads;
    std::vector<uint8_t> isWorking;
    std::queue<std::function<void()>> jobs;

    [[nodiscard]] bool _isBusy() const {
        if (!jobs.empty())
            return true;
        for (const auto threadWorking: isWorking)
            if (threadWorking)
                return threadWorking;
        return false;
    }

    void ThreadLoop(const int i) {
        while (true) {
            std::function<void()> job;
            {
                std::unique_lock<std::mutex> lock(threadMutex);
                isWorking[i] = false;
                joinMutex_condition.notify_all();
                threadMutex_condition.wait(lock, [this] { return !jobs.empty() || shouldStop; });
                if (shouldStop && jobs.empty())
                    return;
                if (!jobs.empty()) {
                    job = jobs.front();
                    jobs.pop();
                    isWorking[i] = true;
                }
            }
            job();
        }
    }

public:
    explicit ThreadPool(const unsigned int numOfThreads = std::thread::hardware_concurrency()) {
        threads.reserve(numOfThreads);
        isWorking.reserve(numOfThreads);
        for (unsigned int i = 0; i < numOfThreads; i++) {
            isWorking.push_back(false);
            threads.emplace_back(&ThreadPool::ThreadLoop, this, i);
        }
    }

    ~ThreadPool() {
        if (!joined)
            Stop();
        threads.clear();
        isWorking.clear();
    }

    template<typename Func, typename... Args>
    void QueueJob(Func job, Args &&... args) {
        {
            std::unique_lock<std::mutex> lock(threadMutex);
            jobs.push(std::bind(job, std::forward<Args>(args)...));
        }
        threadMutex_condition.notify_one();
    }

    bool isBusy() {
        std::unique_lock<std::mutex> lock(threadMutex);
        return _isBusy();
    }

    void Join() {
        std::unique_lock<std::mutex> lock(threadMutex);
        joinMutex_condition.wait(lock, [this]{ return !_isBusy();});
    }

    void Stop() {
        {
            std::unique_lock<std::mutex> lock(threadMutex);
            shouldStop = true;
        }
        threadMutex_condition.notify_all();
        for (std::thread &thread: threads)
            if (thread.joinable())
                thread.join();
        joined = true;
    }

    void ForceStop() {
        {
            std::unique_lock<std::mutex> lock(threadMutex);
            std::queue<std::function<void()>>().swap(jobs);
            shouldStop = true;
        }
        threadMutex_condition.notify_all();
        for (std::thread &thread: threads)
            if (thread.joinable())
                thread.join();
        joined = true;
    }

    static ThreadPool &Instance() {
        static ThreadPool instance;
        return instance;
    }
};

#endif //THREADPOOL_H
